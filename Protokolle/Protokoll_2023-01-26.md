# Pikos Python Kurs am 26.01.2023


## Links
Die Besprechung der Hausaufgabe 1 vom letzten Mal findet sich hier: 
https://diode.zone/w/qVHnAVgjmRfBJVXZ4Ln6hG


## Protokoll
• Hinweise am Anfang 
    ◦ wer noch keine Gruppe hat, kann sie über den Chat suchen
    ◦ keine Abwesenheitsmeldung für den Kurs notwendig 
    ◦ Hausaufgaben sind wichtig für die Praxis-Anwendung – get your hands dirty
    ◦ Haltet es aus, wenn Ihr mal keine Ahnung beim Programmieren habt, genießt es :)
    ◦ Die kommenden Hausaufgaben bauen auf denen der letzten Woche auf. Die Programme werden komplexer 
• Fragen zum Video? Nein.
• Hausaufgabenabfrage – seid Ihr zufrieden?
    ◦ Zu Aufgaben 1 kommt ein Video
    ◦ Besprechung der Aufgabe 2 (Bespr. Aufg. 2 Pikos Skript)
        ▪ Guckt Euch evtl. nochmal die Funktionen begin_fill() und end_fill() an
        ▪ Geeksforgeeks.org ist ein hilfreiche Online-Ressource
    ◦ Besprechung der Aufgabe 3.1 (Bespr. Aufg. 3 Pikos Skript)
        ▪ Funktionen haben Argumente in dieser Form: Funktion(Argument)
        ▪ Erst wird der Variablenname festgelegt (Was auf der Box draufsteht), danach muss dieser vor Ausführen der Funktion definiert werden (etwas muss in die Box reingetan werden) damit die Funktion ausgeführt werden kann und keine Fehlermeldung kommt
        ▪ Das erste Mal taucht der Variablenname für das Argument (Box ohne Inhalt) bei der Definition (def) der Funktion auf 
        ▪ Schreibt Euch die def dreieck(seitenlaenge) Syntax aus der Hausaufgabe (Aufg. 3.1) als Spickzettel auf, da ist der Aufbau gut ersichtlich
    ◦ Besprechung der Aufgabe 3.2 (Bespr. Aufg. 3.2 Pikos Skript)
        ▪ Hier ist das Argument mit dem Variablennamen farbe noch nicht definiert (es ist nichts in der Box), deshalb gibt thonny eine Fehlermeldung aus
• Aufbauend auf Hausaufgabe 3.2: Wie kann nicht nur die Farbe, sondern auch die Seitenlänge verändert werden?  im Code: fd(seitenlaenge), in der Funktionsdefinition: gefuelltes_dreieck(farbe, seitenlaenge)
• Aufbauend auf Hausaufgabe 2 (2.1): Erweitert die Kachelfunktion so, dass die Seitenlänge variabel ist (siehe wieder Bespr. Aufg. 2 Pikos Skript)
    ◦ in der Funktionsdefinition: def kachel(seitenlaenge), im Code: bei allen / bzw. for-schleife nur einmal fd(seitenlaenge) statt einer Zahl
    ◦ vergesst nicht, dies in den verschachtelten Funktionen innerhalb der Funktionen auch zu ändern
    ◦ guckt euch die Fehlermeldungen an, die eine Zeilenzahl angibt, in der möglicherweise ein Argument fehlt 
• ihr könnt in eine default-Funktion „reingucken“ – welche Argumente mit welchen Variablennamen hinterlegt sind – mit help(die_funktion_die_euch_interessiert)
• die Begriffe Argument und Parameter werden austauschbar verwendet (es gibt einen Unterschied, der für uns irrelevant ist)
• „what happens in Vegas, stays in Vegas” – Stichwort Scope. Wenn in def funktion() etwas definiert wird, dann passiert es erstmal NUR im Code unter def funktion(): , und nicht im gesamt Code – führt gerne zu Verwirrungen 
• Aufbauend auf Hausaufgabe 2.2 (siehe wieder Bespr. Aufg. 2 Pikos Skipt): Baut in die random Funktion, die eins der 4 Kacheln auswählt, ein Argument mit Variablennamen ein. 

• Neue Funktion: random.choice(„string, dann gibt Python einen Buchstaben aus“, oder [„L“,“I“,“S“,“T“,“E“]) – bei der Liste machen die eckigen Klammern die Liste aus, die runden Klammern sind die „Ärmchen“ der Funktion
• Abschließende Fragen / Hinweise
    ◦ Die Hausaufgaben werden immer Kunst-generativer :)
    ◦ Veranstaltungshinweise haecksen: 
        ▪ Podcasttreffen 
        ▪ Haecksenfrühstück – eine gute Gelegenheit, die haecksen kennenzulernen
    ◦ Antwort auf eine Frage aus dem Chat: Man kann counter in Funktionen einfügen, damit nach beispielsweise 5x Ausführung einer Funktion etwas anderes passiert – mit if … else counter = counter +1 


## Pikos Datei
```python
from turtle import *

def dreieck():
    for i in range(3):
        fd(100)
        lt(120)
        
dreieck()
    
def dreieck(seitenlaenge):
    for i in range(3):
        fd(seitenlaenge)
        lt(120)

dreieck(200)



# 
# 
# print("Hallo Welt!")


# def dreieck(seitenlaenge):
# dreieck(200)
seitenlaenge = 200
for i in range(3):
    fd(seitenlaenge)
    lt(120)
```
```python
from turtle import *

def gefuelltes_dreieck(farbe, seitenlaenge):
    color(farbe)
    begin_fill()
    for i in range(3):
        fd(seitenlaenge)
        lt(120)
    end_fill()

gefuelltes_dreieck((0.5, 0.5, 0), 20)
```
```python
from turtle import *
import random

# speed(1)


def ecke_mit_fuellung(kantenlaenge):
    begin_fill()
    fd(kantenlaenge)
    lt(90)
    fd(kantenlaenge)
    end_fill()
    

def kachel1(seitenlaenge):
    ecke_mit_fuellung(seitenlaenge)
    lt(90)
    fd(seitenlaenge)
    lt(90)
    fd(seitenlaenge)
    lt(90)

def kachel2():
    fd(100)
    lt(90)
    ecke_mit_fuellung()
    lt(90)
    fd(100)
    lt(90)

def kachel4():
    lt(90)
    fd(100)
    lt(180)
    ecke_mit_fuellung()
    lt(90)
    fd(100)
    lt(90)
    fd(100)
    lt(90)
    fd(100)
    lt(90)

# Baut random_kachel() um, sodass es ein Argument übernimmt.
# das soll eines von den folgenden fünf sein:
#     - "zufall"
#     - "ro"
#     - "lo"
#     - "ru"
#     - "lu"
#     Je nach Argument soll random_kachel() unterschiedl. Dinge tun:
#         Bei "zufall" soll es eine zufällige kachel auswählen.
#         bei "ro" soll es die kachel malen, die rechts oben (ro) schwarz ist
#         bei "lo" die, die links oben schwarz ist
#         bei "ru" die, die rechts unten schwarz ist
#         bei "lu" die, die links unten schwarz ist
#         Bonus: Wenn keine der fünfen passt, soll es dazu eine Info printen.

def random_kachel(position):
    # "zufall", "ro" => kachel2(), "lu" => kachel4()
    if position == "zufall":
        position = random.choice(["ru", "ro", "lo", "lu"])
    
    if position == "ru":
        kachel1(30)
    if position == "ro":
        kachel2(30)    
    if position == "lo":
        kachel1(30)
    if position == "lu":
        kachel4(30)
    if position not in (["ru", "ro", "lo", "lu"]):
        print("Position not in available position list:" position)
        
        
def random_kachel2(position):
    # "zufall", "ro" => kachel2(), "lu" => kachel4()
    if position == "zufall":
        position = random.choice(["ru", "ro", "lo", "lu"])
    
    counter = 0
    if position == "ru":
        kachel1(30)
    else:
        counter = counter + 1
    if position == "ro":
        kachel2(30)
    else:
        counter = counter + 1    
    
    if position == "lo":
        kachel1(30)
    else:
        counter = counter + 1
    
    if position == "lu":
        kachel4(30)
    else:
        counter = counter + 1
        
    if counter == 4: 
        print("Position not in available position list:" position)
    
    
random_kachel("zufall")
```