# Aufgaben am 22.12.2022

**Wenn Ihr Fragen zu den Lösungen habt, dann schreibt gerne an Piko; dann kann es hier mehr Hinweise geben und 
Sachen genauer erklären.**

## 1 – Strings zusammenbauen
### Strings in `print()`
Was ist der Unterschied zwischen den folgenden Code-Schnipseln? Was ist der Unterschied im Ergebnis?
```python
print('Jeder', 'Mensch', 'hat', 'das', 'Recht', 'sich', 'zu', 'irren.')

print('Jeder' + 'Mensch' + 'hat' + 'das' + 'Recht' + 'sich' + 'zu' + 'irren.')
```

**Lösung:**
``` 
Jeder Mensch hat das Recht sich zu irren.
JederMenschhatdasRechtsichzuirren.
```
Wenn die einzelnen Argumente mit einem Komma unterteilt sind, wie in der oberen Zeile, dann packt `print()` da ein
Leerzeichen zwischen jedes einzelne dieser Argumente.

Wenn die Argumente mit einem Plus unterteilt sind, werden sie zuerst zu einem langen string zusammengefügt, und erst dann 
an `print()` übergeben. So, als wären es zwei Schritte:
```python
ausgabe = 'Jeder' + 'Mensch' + 'hat' + 'das' + 'Recht' + 'sich' + 'zu' + 'irren.'

print(ausgabe)
```

### Strings und integers in `print()`
Was ist der Unterschied zwischen den folgenden Code-Schnipseln? Was ist der Unterschied im Ergebnis?
```python
print("Wir haben", "heute", 25, "Orangen", "gekauft.")

print("Wir haben" + "heute" + 25 + "Orangen" + "gekauft.")
```
Das erste funktioniert problemlos und gibt den Satz ganz normal aus; wie oben mit Leerzeichen.

Das zweite gibt eine Fehlermeldung: `TypeError: can only concatenate str (not "int") to str`
Das kommt daher, dass Python nicht weiß, wie es Wörter und Zahlen addieren soll. `2 + 3` ist einfach, `"2" + "3"` ist auch klar, aber 
wenn das gemischt wird, ist unklar, wie das addiert werden soll. Um das zu lösen, müssen wir aus `25` `str(25)` oder `"25"` machen. 
Dann geht es auch mit plus.

## 2 – Matheübungsprogramm

```python
antwort = input("Was ist 12 + 11? ")

if antwort == str(23):
    print("Richtig!")
else:
    print("Falsch!")
```

- Was passiert hier? Schaut mal in die Kommandozeile, da könnt Ihr (wegen `input()`) etwas eingeben!
  - Antwort:
  - In der Kommandozeile können wir etwas eingeben, das dann in der Variable `antwort` gespeichert wird – so, als hätten wir sie im Code zugewiesen.
  - Egal, was wir eingegeben haben, auch wenn es nur Zahlen waren: Es wird als string gespeichert.
  - Dann wird im Code das, was in der Variable `antwort` drin ist, mit `"23"` verglichen. 
  - `antwort` muss also ein string sein, der die Ziffern "23" beinhaltet. Es ist kein integer; es ist eben nicht die 
    *Zahl* 23, sondern ein string – deshalb die Anführungszeichen: "23" und das `str(...)`.
  - Wenn es gleich ist, wird "Richtig!" geprintet. Andernfalls "Falsch!"
- Fallen Euch Anwendungsmöglichkeiten ein?
  - Ein Matheübungsprogramm natürlich, in der verschiedene Zahlen (in einem For-Loop?) 
  - Eine (sehr elementare) Passwort-Abfrage
  - Ein Quiz
  - Schreibt weitere Ideen gerne hier rein: https://md.ha.si/R_q51SYFQjuRx5eXA7lgYA#
- Packt die Zahlen (hier 12, 11 und 23) in Variablen, sodass sie ganz am Anfang des Programms definiert werden.

```python
zahl1 = 12
zahl2 = 11
summe = 23

antwort = input("Was ist " + str(zahl1) + " + " + str(zahl2) + "?")

if antwort == str(summe):
    print("Richtig!")
else:
    print("Falsch!")
```

<details> 
  <summary>Tipps </summary>
    Die Variablennamen müssen außerhalb der Anführungsstriche sein, sonst stellt sich Python dumm...
</details>

<details> 
  <summary>Tipps </summary>
    Guckt auch, was in Aufgabe 1 los war! Das `str()` in der Zeile mit dem `if` ist wichtig!
</details>

- Lasst die 23 (also das Ergebnis) automatisiert ausrechnen, sodass Ihr immer nur zwei Zahlen verändern müsst, wenn Ihr andere Aufgaben haben wollt.

<details> 
  <summary>Tipps </summary>
    Die dritte Zahl (im Beispiel 23) ist ja immer die Summe der beiden anderen Zahlen. Die lässt sich also ganz einfach zuweisen: `zahl3 = ...`
</details>

```python
zahl1 = 12
zahl2 = 11
summe = zahl1 + zahl2

antwort = input("Was ist " + str(zahl1) + " + " + str(zahl2) + "?")

if antwort == str(summe):
    print("Richtig!")
else:
    print("Falsch!")
```
- Importiert random und lasst die beiden Zahlen zufällig generieren!
```python
from random import randint

zahl1 = randint(1, 20)
zahl2 = randint(1, 20)
summe = zahl1 + zahl2

antwort = input("Was ist " + str(zahl1) + " + " + str(zahl2) + "?")

if antwort == str(summe):
    print("Richtig!")
else:
    print("Falsch!")
```
oder
```python
import random

zahl1 = random.randint(1, 20)
zahl2 = random.randint(1, 20)
summe = zahl1 + zahl2

antwort = input("Was ist " + str(zahl1) + " + " + str(zahl2) + "?")

if antwort == str(summe):
    print("Richtig!")
else:
    print("Falsch!")
```

<details> 
  <summary>Tipps </summary>
   Im Protokoll findet sich der Code, den ich in der Stunde vorgestellt habe. Da könnt Ihr ganz viel wiederverwenden!
</details>


## 2 – mehr `random`
### 2.1 Fremder Code
Was macht folgendes Programm? Wie funktioniert begin_fill und end_fill?
```python
from turtle import *

fillcolor("red")

begin_fill()
circle(70)  # probiert es auch mit circle(70, 180) !
end_fill()
```

Es malt einen roten Kreis mit schwarzem Rand. `circle(70, 180)` malt einen Halbkreis, der dann auch gefüllt wird.

### 2.2 Quadrate
- Schreibt ein Programm, das zufällig ein schwarzes oder weißes Quadrat malt. Ihr braucht dafür `random.choice(("black", "white"))`
Lösung:
```python
from turtle import *
import random

laenge = 20

fillcolor(random.choice(("black", "red")))  # oder halt "white" statt "red"
begin_fill()
for k in range(4):
    forward(laenge)
    rt(90)
            
end_fill()
```
oder
```python
from turtle import *
import random

laenge = 20
fillcolor(random.choice(("black", "red")))  # oder halt "white" statt "red"

begin_fill()
circle(laenge, 360, 4)
end_fill()
```

- Kopiert Eure Lösung von Aufgabe 3 von letzter Woche (oder auch die Lösung im Protokoll), und ersetzt die Zeile `dot()` mit dem Programm, das die Quadrate malt. 
  - Achtet dabei darauf, dass Ihr in jeder for-Schleife einen anderen Buchstaben verwendet (also erst i, dann j, dann k...), sonst werden die Quadrate scheinbar zufällig verteilt...
  - Ändert die Längen so, dass die Quadrate einander berühren, sodass ein zufälliges Schachbrettmuster entsteht.
```python
from turtle import *
import random

anzahl_spalten = 6  # wie viele Punkte nebeneinander
anzahl_zeilen = 4  # wie viele Punkte übereinander

penup()

laenge = 30

for i in range(anzahl_zeilen):
    for j in range(anzahl_spalten):
        goto(j*laenge, i*laenge)  # das mit goto ist natürlich nur eine Lösung; die andere Variante mit fd() funktioniert genauso.
        
        fillcolor(random.choice(("black", "red")))
        begin_fill()
        for k in range(4):
            forward(laenge)
            rt(90)            
        end_fill()
```
oder
```python
from turtle import *
import random

anzahl_spalten = 6  
anzahl_zeilen = 4

penup()

laenge = 30

for i in range(anzahl_zeilen):
    for j in range(anzahl_spalten):
        goto(j*laenge, i*laenge)
        
        fillcolor(random.choice(("black", "red")))
        begin_fill()
        rt(45)  # damit das Quadrat nicht auf der Spitze steht
        circle(laenge/1.5, 360, 4)  # 1.5 ist nur eine Annäherung; tatsächlich ist es Wurzel 2
        lt(45)   
        end_fill()
```
Wenn Ihr hier die Zeile `lt(45)` vergesst, sieht es so aus:

![](../Abbildungen/Quadratdurcheinander.png)
<details> 
  <summary>Tipps </summary>
    Achtet auf die richtigen Einrückungen.
</details>

<details> 
  <summary>Tipps </summary>
    Um Euch besser zu orientieren, könnt Ihr erstmal "red" statt "white" nehmen, dann seht Ihr die Quadrate immer...
</details>
<details> 
  <summary>Tipps </summary>
    Die import-Statements gehören ganz nach oben.
</details>
<details> 
  <summary>Tipps </summary>
    Wenn Ihr die Orientierung verliert, fangt von dem dot-Programm an und ersetzt die Dots erstmal mit Quadraten in einer 
for-Schleife (oder mit `circle(laenge, 360, 4)` :D). Dafür braucht Ihr auch pendown(). Bringt das zum Laufen. Dann gebt 
allen Quadraten eine rote Füllung. Und erst dann lasst die Füllung zufällig bestimmen.
</details>

## 3 – Regenbogenlösungen
Packt, wenn Ihr möchtet, Eure Lösungen und Screenshots für die Regenbögen hier rein:  
https://md.ha.si/AO2e_MfpSSW033tLJ8cGKg#