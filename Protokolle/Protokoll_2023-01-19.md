# Pikos Python Kurs am 19.01.2023

## Pads

https://md.ha.si/-3ox3qhbSta-rOB-Wrahcw#  

## Protokoll

### Python – Protokoll 19.01.2023
CCC Eventhinweis Easteregg dieses Jahr endlich wieder in Hamburg ??Camp Nagel?? 

 

### Hausaufgabenbesprechung 

    Generell größtenteils verstanden und gut gelöst (aber ein paar hatten auch jeweils Schwierigkeiten) 

 

### Aufgabe 1 

 

### Aufgabe 2 

Probleme: debugging 

 

### Aufgabe 3 

Random Farben 

Immer mehr sollt ihr nur noch eure Ideen selber umsetzen und nur mit kleinen Tipps klar kommen 

```python
From turtle import * 

anzahl_quadrate = 5 

seitenlaenge = 100 

 

#farbwert_rot = random() # in Farbwert_rot ist nur ein Ergebnis drin – das heisst alle Quadrate sind #dann eine Farbe – Deshalb muss die Funktion jedes Mal wieder ausgefuehrt werden und somit in die #“for i Schleife” rein 

#farbwert_gruen = random() 

#farbwert_blau = random() 

 

for i in range(anzahl_quadrate): 

color(farbwert_rot, farbwert_gruen, farbwert_blau) 

 

farbwert_rot = random() 

farbwert_gruen = random() 

farbwert_blau = random() 

 

begin_fill() 

rt(45) 

circle(seitenlaenge/1.4, 360, 4) # Seitenlänge durch Wurzel2 um ein richtiges Quadrat zu haben 

end_fill() 

 

for j in range(4): 

fd(seitenlaenge) 

lt(90) 

end_fill() 

 

pu() 

fd(seitenlaenge + 10) 

pd() 
```

 

Erklärungen dazu: 

 

Füllung:  

    begin_fill() 

    … 

    end_fill() 

Abstand zwischen den Quadraten: 

    penup() / pu() 

    seitenlaenge + 10 --> Abstand von 10 zwischen den Quadraten 

    pendown() / pd() 

Farbe: 

    Color((r, g, b)) -> die Werte müssen randomisiert werden, damit die Farbe, die aus rot, grün und blau gemischt wird, dann auch zufällig ist 

    --> “Python Kommazahl random zwischen 0 und 1” --> import random --> random.random() 

    Color((random(), random(), random())) 

 

    random.random() 

    --> erstes Random ist die Kiste, zweites ist das Legoteilchen 

    --> zweites random ist das Modul 

 

    Unterschiedliche, aber zufällige Farben: 

    farbwert_rot = random() 

    farbwert_gruen = random() 

    farbwert_blau = random() 

    Dieser Block wird nur ein mal ausgewertet: in “farbwert_rot” ist nicht der random Wert, sondern schon das Ergebnis drinnen 

    Wenn dieser Block (eingerückt) unter “for I in range (anzahl_quadrate):” eingefügt wird, wird die random Funktion für jeden Durchlauf der Schleife, also für jedes einzelne Quadrat, neu ausgeführt --> unterschiedliche Farben  

 

Im Kreis: 

    Begin_fill() 

    Rt(45) --> damit die Vierecke nicht auf einer Spitze stehen 

    Circle(seitenlaenge, 360, 4) 

    Lt(45) --> damit man sich wieder zurück dreht 

    End_fill() 

ABER: der Kreis nimmt Radius: seitenlaenge/1.4 (muss durch die Wurzel von 2 geteilt werden, wegen Pythagoras) 

--> sqrt(2) 

 

 

### Inhalt 

Ungefähr das gleiche wie in Aufgabe 3, aber so, dass die Quadrate an unterschiedlichen Stellen sind, aber wieder mit zufälligen Farben 

### 1 Aufgabe: 
lasst (ohne die äußere for-Schleife) vier der fünf zufällig gefärbten Quadrate an den vier Ecken der Leinwand und das fünfte in der Mitte zeichnen.  

https://md.ha.si/-3ox3qhbSta-rOB-Wrahcw#  

Die Turtle erreicht den Ausgangspunkt, um die Quadrate zu malen, mit goto(). Da kommen direkt Koordinaten (zwei Zahlen) rein, wo genau Ihr hin wollt (also welche Koordinaten es genau sind), müsst Ihr ausprobieren. 

 

Für die Position: Koordinaten 

    Goto([x-Koordinate], [y-Koordinate]) 

    z.B.: goto(300, 300) 

    pu() 

    goto(300, 300) 

    pd() 

    Für die verschiedenen Kombinationen von 300 und –300 für x- und y-Koordinate, sowie 0/0 

 

Funktionsdefinitionen 

Def buntes_quadrat(): --> dem Computer gesagt, dass er immer alles ausführen soll, was untendrunter eingerückt ist, wenn ein buntes Quadrat gemacht werden soll 

--> damit kann das immer einfach wiederholt werden, wenn die neuen Koordinaten gegeben werden, aber in sehr kurz und elegant 

--> die gesamte Erklärung muss nur einmal gegeben werden, hinterher wird dann einfach gesagt “das gleiche wie vorhin jetzt nochmal” 

--> Metapher: Backrezept 

    man hat die Anleitung zwar schon, aber ja die fertigen Brownies noch nicht, nur weil man weiß, wie es  geht 

    Anleitung muss nur an der richtigen Stelle ausgeführt werden 

    “wie gehen an genau diese Stelle und machen da ein buntes Quadrat” 

Klammern: 

    Es gibt Funktionen, die in den () etwas haben und welche ohne –-> die, bei denen in den Funktionen etwas drinnen steht, dass sie dann verändert, kommt nächste Woche! 

 

### Aufgabe 2: Funktionsdefinition, die ein Dreieck baut 

“Rezept”: Definition für das Dreieck –-> def dreieck() 

    Funktion kriegen wir, indem wir “def” davor schreiben und alles drunter einrücken 

    Untendrunter wieder dreieck() schreiben, “damit der Kuchen auch gemacht wird”, damit die Funktion auch ausgeführt wird und da nicht nur steht, wie das theoretisch ginge 

Gefülltes Dreieck: 

    Forschleife rein: for i in range(3): 

Die gefetteten, lilafarbenen Wörter sind für Python reservierte Begriffe 

    Gibt nicht viele davon 

    Zeile endet mit Doppelpunkt, danach kommt üblicherweise ein indent --> der Definitionskörper ist nach dem Definitionskopf etwas eingerückt 

 
```python
seitenlaenge = 100 

  

def dreieck(): 
    fd(100) 
    lt(120) 
    fd(100) 
    lt(120) 
    fd(100) 
    lt(120) 
  
def gefuelltes_dreieck(): 
    begin_fill() 

    for i in range(3): 
        fd(100) 
        lt(120) 
    end_fill() 
     
gefuelltes_dreieck() 

# Aus den verschiedenen Rezepten kann man auch ein ganzes Rezeptebuch schreiben 
# und die vers. einzelnen Rezepte kombinieren...
# Wichtig nur nicht das Backen/Kochen vergessen = Ihr muesst die Funktion auch aufrufen 

def  gefuelltes_dreieck():
    begin_fill() 
    dreieck() 
    end_fill() 
```
 

Funktionsdefinition anwenden: statt bei def gefuelltes_dreieck() nochmal zu wiederholen, wie ein dreieck gemacht wird, kann man auch einfach def dreieck() einfügen --> quasi ein Grundelement im Rezept nutzen, aber es muss nicht noch mal lange erklärt werden, wie z.B. Mürbeteig gemacht wird 

 

### Aufgabe 3: Würfelfunktion 

Wuerfeln() --> sollen jedes Mal unterschiedliche Zahlen sein, zwischen 1 und 6 

 
```python
from random import randint 

#--> randint als einzelnes Legostückchen genommen, kann einzeln verwendet werden 

#Sonst ganze Baukastenbox 

#-->import random 

  

def wuerfeln(): 
    zahl = randint(1,6) # verlangt zwei attribute (von, bis) 
    print(zahl) 

  

wuerfeln() #backen nicht vergessen
```



## Pikos Datei
Hausaufgabe
```python
from turtle import *
from random import random
from math import sqrt # "sqareroot"

anzahl_quadrate = 5
seitenlaenge = 100

for i in range(anzahl_quadrate):
#     color((random(), random(), random()))
    
    farbwert_rot = random()
    farbwert_gruen = random()
    farbwert_blau = random()


    color((farbwert_rot, farbwert_gruen, farbwert_blau))
    
    
    begin_fill()
    rt(45)
    circle(seitenlaenge/sqrt(2), 360, 4)
    lt(45)
    end_fill()
    
    penup()
    fd(seitenlaenge + 10)
    pendown()
```


Aufgabe mit den Quadraten in den Ecken; in mühsam:
```python
from turtle import *
from random import random
from math import sqrt # "sqareroot"

anzahl_quadrate = 5
seitenlaenge = 100

pu()
goto(300, 300)
pd()



farbwert_rot = random()
farbwert_gruen = random()
farbwert_blau = random()
color((farbwert_rot, farbwert_gruen, farbwert_blau))
begin_fill()
rt(45)
circle(seitenlaenge/sqrt(2), 360, 4)
lt(45)
end_fill()
penup()
fd(seitenlaenge + 10)
pendown()


pu()
goto(-300, 300)
pd()



farbwert_rot = random()
farbwert_gruen = random()
farbwert_blau = random()
color((farbwert_rot, farbwert_gruen, farbwert_blau))
begin_fill()
rt(45)
circle(seitenlaenge/sqrt(2), 360, 4)
lt(45)
end_fill()
penup()
fd(seitenlaenge + 10)
pendown()


pu()
goto(-300, -300)
pd()



farbwert_rot = random()
farbwert_gruen = random()
farbwert_blau = random()
color((farbwert_rot, farbwert_gruen, farbwert_blau))
begin_fill()
rt(45)
circle(seitenlaenge/sqrt(2), 360, 4)
lt(45)
end_fill()
penup()
fd(seitenlaenge + 10)
pendown()


pu()
goto(300, -300)
pd()



farbwert_rot = random()
farbwert_gruen = random()
farbwert_blau = random()
color((farbwert_rot, farbwert_gruen, farbwert_blau))
begin_fill()
rt(45)
circle(seitenlaenge/sqrt(2), 360, 4)
lt(45)
end_fill()
penup()
fd(seitenlaenge + 10)
pendown()


pu()
goto(0, 300)
pd()



farbwert_rot = random()
farbwert_gruen = random()
farbwert_blau = random()
color((farbwert_rot, farbwert_gruen, farbwert_blau))
begin_fill()
rt(45)
circle(seitenlaenge/sqrt(2), 360, 4)
lt(45)
end_fill()
penup()
fd(seitenlaenge + 10)
pendown()
```
... und in elegant, mit einer Funktionsdefinition:
```python
from turtle import *
from random import random
from math import sqrt # "sqareroot"

seitenlaenge = 100

def buntes_quadrat():
    pd()

    farbwert_rot = random()
    farbwert_gruen = random()
    farbwert_blau = random()
    color((farbwert_rot, farbwert_gruen, farbwert_blau))
    begin_fill()
    rt(45)
    circle(seitenlaenge/sqrt(2), 360, 4)
    lt(45)
    end_fill()
    penup()
    fd(seitenlaenge + 10)
    pendown()

    pu()



pu()
goto(300, 300)

buntes_quadrat()

goto(-300, 300)

buntes_quadrat()

goto(-300, -300)

buntes_quadrat()

goto(300, -300)

buntes_quadrat()

goto(0, 0)

buntes_quadrat()
```

Verschiedene selbstdefinierte Funktionen
```python
from turtle import *
from random import randint
# ungefülltes Dreieck als Funktion


def dreieck():
    fd(100)
    lt(120)
    fd(100)
    lt(120)
    fd(100)
    lt(120)


def gefuelltes_dreieck():
    begin_fill()
    for i in range(3):
        fd(100)
        lt(120)
    end_fill()



def gefuelltes_dreieck():
    begin_fill()
    dreieck()
    end_fill()


# gefuelltes_dreieck()
# 
# hui = "lalala"

def wuerfeln():
    zahl = randint(1, 6)
    print(zahl)

wuerfeln()  # => printet eine Zufallszahl zwischen 1 und 6
wuerfeln()
```