# Aufgaben am 29.12.2022


## 1 – Unzuverlässiger Tageslichtwecker
Liebe Freunde möchten einen Tageslichtwecker bauen. Der Wecker steuert eine Lampe, die dann in Schritten immer 
heller wird. Das Weckerprogramm wird immer genau um Mitternacht gestartet, wartet dann z.B. acht Stunden, und sorgt 
dann dafür, dass die Lampe heller wird.

### 1.1 Verpasste Startzeit
Sie sind mit dem Projekt noch nicht ganz fertig; im Moment sieht es so aus: Manchmal, so in 40% der Fälle, 
funktioniert er gar nicht. 

Schreibt ein Python-Programm, das das derzeitige Verhalten des Weckers nachahmt. Die Lampe müsst Ihr mit einer 
Turtle-Zeichnung ersetzen. Sucht gerne im Netz nach einer Python Turtle-Funktion, die die Hintergrundfarbe setzt!

<details> 
  <summary>Tipps </summary>
   Wir hatten in der letzten Stunde ja die Module `time` und `random` kennengelernt. Schaut den Code an und überlegt,
was Ihr wiederverwenden könnt!
</details>

<details> 
  <summary>Tipps </summary>
   `time` muss acht Stunden warten, also muss die Funktion `sleep()` mit eine recht hohen Sekunden-Anzahl aufgerufen 
werden. Am allerelegantesten ist es, ganz am Anfang die Stunden in eine Variable zu speichern (oder sogar mit 
`input` abzufragen!) und dann die Sekundenanzahl auszurechnen...
</details>


<details> 
  <summary>Tipps </summary>
   Um das Programm zu testen, ist es sinnvoll, nicht die ganzen acht Stunden zu warten, sondern einige wenige 
Sekunden in das `time.sleep()` zu tun.
</details>


<details> 
  <summary>Tipps </summary>
   In einem `if`-Statement kann eins nicht nur "a == b" schreiben, sondern auch "a > b" oder "a < b"! Das ist in 
unserem Fall sehr nützlich.
</details>

<details> 
  <summary>Tipps </summary>
   Um besser zu verstehen, wie Ihr `random` in diesem Fall verwenden könnt, könnt Ihr hier die ersten beiden 
Antworten anschauen: https://stackoverflow.com/questions/3203099/percentage-chance-to-make-action 
1. `random.random` gibt uns eine zufällige Zahl zwischen 0 und 1 aus. Die ist mit 36% Wahrscheinlichkeit kleiner als 
0.36, und mit 64% Wahrscheinlichkeit größer.
2. Eine 36%-Chance bedeutet, dass, wenn wir 100 mal würfeln, 36 Würfelwürfe "zutreffen" und 64 nicht "zutreffen". 
</details>

### 1.2 Langsames Hellerwerden
Aber auch beim Hellerwerden (wenn es denn überhaupt heller wird...) gibt es noch Probleme: Eigentlich sollte es alle 
zwei Sekunden einen Schritt heller werden.
Leider klappt das nicht: Manchmal braucht es auch mal eine Minute zum Hellerwerden.

Für das allmähliche Hellerwerden braucht Ihr natürlich viel mehr Farben. Schaut mal, wie die Farben bei folgendem 
Code-Schnipsel gemacht werden!

```python
from turtle import *
pensize(20)

color((1, 0, 0))
forward(50)

color((0, 1, 0))
forward(50)

color((0, 0, 1))
forward(50)

color((1, 1, 0))
forward(50)

color((0, 0.5, 0.5))
forward(50)

color((0.2, 0.9, 0.1))
forward(50)
```

<details> 
  <summary>Tipps </summary>
   Ihr braucht eine for-Schleife, die einmal die Background-Color setzt und dann eine zufällige Zeit zwischen 2 
Sekunden und 2 Minuten wartet. (Auch hier ist es sinnvoll, die Zeiten etwas zu verkürzen, damit Ihr beim 
Ausprobieren nicht so super viel warten müsst...
</details>

<details> 
  <summary>Tipps </summary>
   `color()` nimmt also auch ein Zahlentupel (also eine Auflistung von Zahlen, die mit runden Klammern eingefasst 
ist), an. Je niedriger die Zahlen sind, desto dunkler ist die Farbe. Die höchste Zahl ist 1. Die Farbe wird 
zusammengemischt aus drei Grundfarben: rot, grün und blau; genau in dieser Reihenfolge. Diese Zahlen können auch aus 
den Zahlen einer for-Schleife berechnet werden! 
</details>

<details> 
  <summary>Tipps </summary>
   Das Zahlentupel könnte zum Beispiel zuerst (0, 0, 0) (also schwarz) sein, dann (0.1, 0.1, 0.1), dann (0.2, 0.2, 0.
2) und so weiter. Wie könnt Ihr diese Zahlen ausrechnen, wenn Ihr in der for-Schleife in der Variablen i erstmal 0 
habt, dann 1, dann 2, und so weiter?
</details>

Herzlichen Glückwunsch! Ihr habt jetzt einen kaputten "Tageslicht"-"Wecker"! \o/

## 2 – input
Schreibt ein kurzes Quiz, das mit `input()` eine Frage stellt die Antwort überprüft. Das Ergebnis soll so aussehen:

![](../Abbildungen/Quiz_Beispiel.png)

Mehr Infos zu `input()` findet Ihr in den Lösungen zu den letzten Hausaufgaben. 

## 3 - Fremder Code

### 3.1 Korrigieren

```python
# Ein Zahlenrat-Spiel!

from random import randint

geheime_zahl = random.randint(1, 1000)

for i in range(7)
    geratene_zahl = input("Rate eine Zahl zwischen 1 und 100! ")
    if geratene_zahl = geheime_zahl:
        print("Richtig! Du hast die geheime Zahl erraten!")
        print("Du hast"; i; "Versuche gebraucht.")
        break  # Dieses Statement beendet die for-Schleife
    if geratene_zahl > geheime_zahl:
        prnit("Du hast zu niedrig geraten!")
    if geratene_zahl < geheime_zahl:
        print("Du hast zu niedrig geraten!")
```

### 3.2 Verstehen

```python
for i in range(12):
    print("i ist:", i)
    
    print("i%2 ist:", i%2)
    if i % 7 == 0:
        print("Potzblitz!")
    print()
```
Was macht der Code? Was macht nochmal `%`?